== Character Creation
---
=== Steps
[cols="2", frame=none, grid=none]
|===
a|. Name your character
. *Health Points* start at 10
. *Inventory Score* starts at 8
. Roll or choose from one option from each of the *Character Tables*. *Animal Table*, *Childhood Table*, and *Profession Table* 
. Choose an appropriate *Animal Trait* that the character has retained, with Game Master approval.  
* Exp: A cat character may have retractable claws or low light vision.
. Based on the Childhood and Profession determine the characters *starting Ability Scores and gear*
a| *Additional Starting Gear*

* 2 rations
* a set of clothing
* 1 scrap, or insect weapon or an item of choice(with game master approval)

*Ties that Bind:* Each player selects a different PC from the one they are  playing and then the players of both characters describe something that ties the two characters together.  Are the characters related, childhood friends, frenemies...
|===

=== Character Tables

.*Animal(Mammals):* What kind of animal is your character? *Roll d66*
[cols="3*", stripes=all]
|===
l|11. Wolf
12. Badger
13. Fox
14. Racoon
15. Opossum
16. Squirrel

l|21. Chipmunk
22. Deer
23. Buffalo
24. Otter
25. Camel
26. Lynx

l|31. Bear
32. Elk
33. Moose
34. Mouse
35. Rat
36. Ferret

l|41. Dogs
42. Cats
43. Cattle
44. Hamster
45. Goat
46. Sheep

l|51. Horses
52. Pigs
53. Guinea Pig
54. Penguin
55. Kangaroo
56. Platypus

l|61. Chimpanzee
62. Gorilla
63. Zebra
64. Rhino
65. Lion
66. Tiger
|===

<<<
.*Childhood:* Where did your character grow up? *Roll a 1d6*
[cols="3"]
|===
a| (1) In the Woods (+1 Prowl)::
* Handcrafted Item of Choice
(2) On the Frontier (+1 Brainy)::
* Axe
a| (3) Nomad/Traveler (+1 Prowl)::
* Flint & Steel
(4) Villager (+1 Feral)::
* Hammer & Saw
a| (5) Townsfolk (+1 Brainy)::
* Book
(6) Abandoned Human City (+1 Feral)::
* Scrap Item of Choice
|===

.*Failed Professions:* This is what the player did for a living before something caused them to choose to become a forager. *Roll a 2d6*
[cols="2"]
|===
a| (2) Militia (+1 Feral/ +1 Prowl):: 
* What was your specialty? (cook, scout, officer, grunt..)
* Goon-made musket, sword, polearm, or a bow
* A Canteen
(3) Secret Martial Arts Order  (+1 Feral/ +1 Prowl):: 
* Why is your order a secret?
* Goon-made Martial Art Weapon
* Something Personal (Item of Choice)
(4) Laborer (+1 Feral/+1 Prowl):: 
* What kind of laborer did you do?
* Waterskin
* Shovel
(5) Farmer(Herder)  (+1 Feral/ +1 Brainy):: 
* What did you farm?
* Farm tool (Item of Choice)
* Farm insect (Insect of Choice)
(6) Hunter (+1 Feral/ +1 Prowl):: 
* What did you hunt?
* Goon-made Hunting Weapon (single shot rifle, bow, trap…)
* Hunting insect (Insect of Choice)
a| (7) Crafter(+1 Brainy/ +1 Prowl)::
* What did you craft?
* Craft Tools (appropriate for type of craft)
* Something you crafted (Item of Choice)
(8) Trader (+1 Brainy/ +1 Prowl)::
* What did you trade?
* Compass
* Something to trade (Item of Choice)
(9) Technician (+1 Brainy/ +1 Prowl)::
* What is your technical specialty?
* Technician Tools (appropriate for type of technician)
* Technical Manuel
(10) Cop (+1 Feral/ +1 Prowl)::
* Whose laws did you enforce?
* Goon-made revolver
* Something Confiscated (Item of Choice)
(11) Bandit  (+1 Feral/ +1 Prowl)::
* How did you become a bandit?
* Roll once on weapon table
* Something Old (Item of Choice)
(12) Thief  (+1 Feral/ +1 Brainy)::
* What did you like to steal? 
* Lock picks
* Something Stolen (Item of Choice)
|===
