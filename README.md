<!-- PROJECT SHIELDS -->
<!-- [![MIT License][license-shield]][license-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">After the Goons</h3>

  <p align="center">
    A Post-Apocalyptic Mutant Animal TTRPG
        <br />
    <a href="https://krmlab.itch.io/after-the-goons"><strong>Download the PDF»</strong></a>
    <br />
  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">Built with</a></li>
    <li><a href="#acknowledgment">Built with</a></li>
    <li><a href="#prerequisites">Prerequisites</a></li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## Built with


[![AsciiDoc][asciidoc.org]][Asciidoc-url]

[![Asciidoctor][asciidoctor.org]][Asciidoctor-url]

[![Vale][vale.sh]][Vale-url]

## About the project
- A tabletop roleplayinng game where you play as mutant animal in a post-apocalyptic world

## Acknowledgment
*After the Goons is a hack of Tunnel Goons by Nate Treme (Highland Paranormal Society)*
a Creative Commons Attribution 4.0 Licensed product. Find out more at https://tunnelgoons.com and https://natetreme.com

*Additional Inspiration from:*

* 24xx by Jason Tocci at https://pretendo.games
* Cairn by Yochai Gal at https://cairnrpg.com
* Teenage Mutant Tunnel Goons by JP Coovert at https://youtube.com/jpcoovert and https://jpcoovert.com

## Prerequisites

The things you need before installing the software.

* NPM
* Node
* AsciiDoctor

## Build

A step by step guide that will tell you how to get the development environment up and running.
Build is located in the documentation-cafe/docCafeSite/build/site folder

```
$ gitclone
```


## License
 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">After the Goons © 2023</span> by <span property="cc:attributionName">KRM (IvyCap Labs)</span> is licensed under <a href="https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Creative Commons Attribution 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""></a></p>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/IvyCap/dungeonbuilder/blob/main/LICENSE -->
[asciidoc.org]: https://img.shields.io/badge/Asciidoc-0769AD?style=for-the-badge&logo=asciidoc&logoColor=white
[Asciidoc-url]: https://asciidoc.org/
[asciidoctor.org]: https://img.shields.io/badge/Asciidoctor-DD0031?style=for-the-badge&logo=asciidoctor&logoColor=white
[Asciidoctor-url]: https://asciidoctor.org/
[antora.org]: https://img.shields.io/badge/Antora-FF2D20?style=for-the-badge&logo=antora&logoColor=white
[Antora-url]: https://antora.org/
[vale.sh]: https://img.shields.io/badge/Vale-99d98c?style=for-the-badge&logo=vale&logoColor=black
[Vale-url]: https://vale.sh/
